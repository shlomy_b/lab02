//Shlomo Bensimhon 
//1837702
package lab02.eclipse;

public class BikeStore {
	public static void main(String[] args) {
		
		Bicycle[] b1 = new Bicycle[3];
		b1[0] = new Bicycle("Kawasaki", 5, 180.0);
		b1[1] = new Bicycle("Honda", 6, 190.0);
		b1[2] = new Bicycle("Triumph", 6, 200.0);
		b1[0] = new Bicycle("Bmw", 6, 220.0);
		
		for(Bicycle bikes: b1) {
			System.out.print(bikes.toString());
			System.out.print("\n");
			System.out.print("----------------------");
			System.out.print("\n");
		}
	}
}
